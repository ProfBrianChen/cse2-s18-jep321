// Jeffery Park 
// Lab07 
// jep321
import java.util.Random;
import java.util.Scanner;

public class Lab07{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    
    // I call each of the methods in the main method for phase 1 of lab
    String adjective = "";
    String subject = "";
    String verb1 = "";
    String object = "";
    String adjective1 = "";
    
    
     adjective = adj(adjective);
     subject = sub(subject);
     verb1 = verb(verb1);
     adjective1 = adj(adjective1);
     object = obj(object);
   
  System.out.println("The " + adjective + " " + subject + " " + verb1 + " " + "the " + adjective1 + " " + object + ".");
    //From here I am doing phase 2 of the lab
    String verb2 = "";
    String verb3 = "";
    String object2 = "";
    String adjective2 = "";
    String subject2 = "";
    String object3 = "";
    String object4 = "";
    
    verb2 = verb(verb2);
    verb3 = verb(verb3);
    object2 = obj(object2);
    object3 = obj(object3);
    object4 = obj(object4);
        
    
    System.out.println("It " + verb2  + " the " + object2 + " and the " + object3);
    System.out.println("That " + subject + " " + verb3 + " its " + object4 + "!");  
   
    String sentence1 = "";
    String sentence2 = "";
    String sentence3 = "";
    String sentence4 = "";
    String sentence5 = "";
    
    sentence1 = sentence(sentence1);
    sentence2 = sentence(sentence2);
    sentence3 = sentence(sentence3);
    sentence4 = sentence(sentence4);
    sentence5 = sentence(sentence5);
    
    System.out.println(sentence1 + sentence2 + sentence3 + sentence4 + sentence5);
    
    
    System.out.print("Would you like another sentence? 1 for yes, 2 for no. ");
    int answer = input.nextInt();
      if (answer == 1){
        Lab07.main(null);
      }
      if (answer == 2){
        System.exit(0);
      }
}
  //I start writing the methods here for phase 1 of lab 
  public static String adj(String ad){
    Random randomGenerator = new Random();
     ad = "";
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        ad = "quick" ;
          break;
      case 1:
        ad = "slow";
        break;
      case 2:
        ad = "brown";
        break;
      case 3:
        ad = "ugly";
        break;
      case 4:
        ad = "cool";
        break;
      case 5:
        ad = "hot";
        break;
      case 6:
        ad = "smelly";
        break;
      case 7:
        ad = "crazy";
        break;
      case 8:
        ad = "noisy";
        break;
      case 9:
        ad = "sad";
        break;    
    }
    return ad;
  }
  public static String sub(String sb){
    Random randomGenerator = new Random();
     sb = "";
    int randomInt = randomGenerator.nextInt(10);
        switch (randomInt){
      case 0:
        sb = "turtle" ;
          break;
      case 1:
        sb = "fox";
        break;
      case 2:
        sb = "bunny";
        break;
      case 3:
        sb = "jacket";
        break;
      case 4:
        sb = "pencil";
        break;
      case 5:
        sb = "chair";
        break;
      case 6:
        sb = "desk";
        break;
      case 7:
        sb = "paper";
        break;
      case 8:
        sb = "eraser";
        break;
      case 9:
        sb = "phone";
        break;    
    }
    return sb;
  }
  public static String verb(String vb){
        Random randomGenerator = new Random();
     vb = "";
    int randomInt = randomGenerator.nextInt(10);
        switch (randomInt){
      case 0:
        vb = "ran" ;
          break;
      case 1:
        vb = "threw";
        break;
      case 2:
        vb = "passed";
        break;
      case 3:
        vb = "fought";
        break;
      case 4:
        vb = "drove";
        break;
      case 5:
        vb = "walked";
        break;
      case 6:
        vb = "typed";
        break;
      case 7:
        vb = "flicked";
        break;
      case 8:
        vb = "punched";
        break;
      case 9:
        vb = "kicked";
        break;    
    }
    return vb;
    
  }
  public static String obj(String ob){
            Random randomGenerator = new Random();
     ob = "";
    int randomInt = randomGenerator.nextInt(10);
        switch (randomInt){
      case 0:
        ob = "cat" ;
          break;
      case 1:
        ob = "dog";
        break;
      case 2:
        ob = "fish";
        break;
      case 3:
        ob = "whale";
        break;
      case 4:
        ob = "dolphin";
        break;
      case 5:
        ob = "shark";
        break;
      case 6:
        ob = "kangaroo";
        break;
      case 7:
        ob = "crocodile";
        break;
      case 8:
        ob = "parrot";
        break;
      case 9:
        ob = "monkey";
        break;    
    }
    return ob;
    
  }
  public static String sentence(String sent){
    Random randomGenerator = new Random();
     sent = "";
    int randomInt = randomGenerator.nextInt(10);
           switch (randomInt){
      case 0:
        sent = "We went to a store. " ;
          break;
      case 1:
        sent = "We walked to the park. ";
        break;
      case 2:
        sent = "Is this how you set up this program? ";
        break;
      case 3:
        sent = "I went to grab lunch at 12:00PM. ";
        break;
      case 4:
        sent = "This lab is so confusing. ";
        break;
      case 5:
        sent = "Will I get a good grade in this class? ";
        break;
      case 6:
        sent = "My calculus exam is next Thursday. ";
        break;
      case 7:
        sent = "I am glad that I am almost finished with this program. ";
        break;
      case 8:
        sent = "That is if I am doing it correctly. ";
        break;
      case 9:
        sent = "I just want a good grade in this class. ";
        break;    
    }
    return sent;
  }
  }