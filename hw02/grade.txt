Grade:     100/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes, There is no compiler erorrs.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
N/A
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
All good so far, just include comments. 
E) Other comments:
Improve Code alignment to be more readable. 
Add a comment in the top with your name, lehigh Id, and assignment number
