import java.util.Scanner; //Must import java utility first
//or else there will be a compiler error

public class Check{
    			//Start of every java program
   			public static void main(String[] args) {
          
Scanner myScanner = new Scanner( System.in ); //declares the Scanner tool to be used
          //"System In" goes into the program.
          System.out.print("Enter the original cost of the check in the form xx.xx: ");
double checkCost = myScanner.nextDouble();
          //Allows the user to type in any number of the form xx.xx
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
double tipPercent = myScanner.nextDouble();
          //Allows user to type any tip percentage as a whole number
tipPercent /= 100; 
          //converts Tip to decimal format
System.out.print("Enter the number of people who went out to dinner: ");
int numPeople = myScanner.nextInt();
          //Allows user to type in any number of people who went to dinner
          //We want it to be integer since there cant be .5 of a person
double totalCost;
double costPerPerson;
int dollars, //Amount of dollars of the cost
          dimes, pennies; //Stores digits to the right of decimal point for $cost
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
          //Divides the total amount by the number of people, drops decimal fraction
          
          dollars = (int)costPerPerson;
            
dimes = (int)(costPerPerson*10) % 10;
          pennies = (int)(costPerPerson*100) %10;
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
          
            
}    //end the Public Static
  	} //end the Class