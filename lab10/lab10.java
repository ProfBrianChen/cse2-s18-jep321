import java.util.Random;
public class lab10{
  
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int[][]array = new int [height][width];
    if(format){
    for (int a = 1; a < height + 1; a++){
      array[a-1] = new int[width];
      for(int b = 1; b < width + 1; b++){
        array[a-1][b-1] = b + (a-1)* width;
      }
    }
    }
    
    if (format == false){
      for(int a = 1; a < height + 1; a++){
        array[a-1] = new int [width];
        for(int b = 1; b < width + 1; b++){
          array[a-1][b-1] = a + (b-1) * width;
        }
    }
  }
    return array;
  }
  
  public static void printMatrix( int[][] array, boolean format ){
    if(format){
      for (int a = 0; a < array.length; a++){
        for (int b = 0; b < array[a].length; b++){
          System.out.printf("%2d ", array[a][b]);
        }
        System.out.println();
      }
    }
    if(format == false){
      for (int a = 0; a < array.length; a++){
        for (int b = 0; b < array[a].length; b++){
          System.out.printf("%2d ", array[a][b]);
        }
        System.out.println();
      }
    }
    
  }
  
  public static int[][] translate(int[][] array){
    int[][]newArray = new int [array[0].length][array.length];
    
    for (int a = 0; a < array.length; a++){
        for (int b = 0; b < array[a].length; b++){
          newArray[a][b] = array[b][a];
        }
    }
    
    return newArray;
  }
  
  public static int[][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb) {
    if(formata == false){
      translate(a);
    }
    if (formatb == false){
      translate(b);
    }
    
    int[][]c = new int [a.length][a[0].length];
    
    if(a.length == b.length && b[0].length == a[0].length){
      for (int y = 0; y < a.length; y++){
        for (int x = 0; x < a[y].length; x++){
          c[y][x] = a[y][x] + b[y][x];
        }
      }
    }
    else {
      System.out.println("The arrays cannot be added");
      return null;
    }
    return c;
  }

  public static void main(String[] args){
   Random rand = new Random();
   int width1 = rand.nextInt(3) + 2;
   int height1 = rand.nextInt(3) + 2;
   int height2 = rand.nextInt(3) + 2;
   int width2 = rand.nextInt(3) + 2;
   
   int[][]A = new int[height1][width1];
   int[][]B = new int[height1][width1];
   int[][]C = new int[height2][width2];
    
    
   boolean format = true;
   
   A = increasingMatrix(width1,height1,format);
   B = increasingMatrix(width1,height1,format);
   C = increasingMatrix(width2,height2,format);
   
   System.out.println();
   System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
   printMatrix(A,format);
   System.out.println();
   
   System.out.println("Generating a matrix with width " + width1 + " and height " + height1 + ":");
   printMatrix(B,format);
   System.out.println();
   
   System.out.println("Generating a matrix with width " + width2 + " and height " + height2 + ":");
   printMatrix(C,format);
   System.out.println();
   
   System.out.println("A + B: ");
   printMatrix(addMatrix(A,format,B,format),format);
   System.out.println();
    
   System.out.println("A + C: ");
   if (addMatrix(A,format,C,format) != null) {
     printMatrix(addMatrix(A,format,C,format),format);
   }
  }
}