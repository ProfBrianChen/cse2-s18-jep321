import java.util.Scanner;//Scanner tool to be used

public class Convert{
  //start of every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //Declares Scanner tool
    System.out.print("Enter the affected Area in acres: "); //Prompts user to type in any value of affected area
    double affectedArea= myScanner.nextDouble(); //Stores the input value as a double to "affectedArea"
    System.out.print("Enter the rainfall in the affected area: "); //Prompts user to type in any value of rainfall
    double rainfall = myScanner.nextDouble(); //Stores the input value as a double to "rainfall"
    
    double hurricane = affectedArea*rainfall; //multiplies both values to get acre-inches of water
    
    double hurricanefeet = hurricane/12; // converts to acre-foot
    
    double hurricanecubeft = hurricanefeet*43560; // converts to cubic feet 
    
    double hurricanecubemi = hurricanecubeft/1.47197952e11; //converts to cubic miles
    
   
    System.out.println("The amount of the rain in affected area in cubic miles is: " + hurricanecubemi + " miles^3"); //prints the final value in terms of cubic miles
    
    
      
    
    
  }// end of class
                         
}