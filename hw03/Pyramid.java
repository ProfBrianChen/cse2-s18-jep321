import java.util.Scanner; // We want to use the Scanner tool here

public class Pyramid{
  //Start of every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in); //Calling the Scanner to be used here
    System.out.print("Enter the base length of the Pyramid: "); //Prompts user to input a value for the base of the 
    //pyramid
    double length= myScanner.nextDouble(); //This will store the input value as a double
    
    System.out.print("Enter the base width of the Pyramid: ");
      double width = myScanner.nextDouble();
    
    System.out.print("Enter the height of the Pyramid: "); //Prompts user to input a value for the height
    //of the pyramid
    double height = myScanner.nextDouble();//Stores the value as a double
    
    double pyramid = ((length*width*height)/3);//Finds the volume of the pyramid
    
    System.out.println("The volume of the pyramid is:" + pyramid + " units^3"); //Prints out the value
    
      
    
    
  }//end of class
                         
}