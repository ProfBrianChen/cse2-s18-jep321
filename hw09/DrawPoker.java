//Jeffery Park
//jep321
//hw09: Draw Poker

import java.util.Random;

public class DrawPoker{
  public static void main(String[] args){
    //0-12 Diamonds 13-25 clubs 26-38 hearts 39-51 spades
    //lowest is ace; highest three: jack queen king
    //I first set up the array for the card deck and player 1 / 2 and their scores
    
    int[] cardDeck = new int[52];
    int[] firstPlayer = new int[5];
    int[] secondPlayer = new int[5];
    Random rand = new Random();
    int x = 0;
    int y = 0;   
    int playerOneScore = 0;
    int playerTwoScore = 0;    
    //I then set each number to the index 
    for(int i = 0; i < 5; i++){
      x = rand.nextInt(52);
      firstPlayer[i] = x;
      y = rand.nextInt(52);
      secondPlayer[i] = y;      
    }
    //I call the each hand method here add the numbers up
    if(pair(firstPlayer) == true){
      playerOneScore += 1;
    }    
    if(pair(secondPlayer) == true){
      playerTwoScore+=1;
    }   
    if(threeKind(firstPlayer) == true){
      playerOneScore+=1;
    }
    if(threeKind(secondPlayer) == true){
      playerTwoScore+=1;
    }   
    if(flush(firstPlayer) == true){
      playerOneScore+=1;
    }    
    if(flush(secondPlayer) == true){
      playerTwoScore+=1;
    }
    if(fullHouse(firstPlayer) == true){
      playerOneScore+=1;
    }
    if(fullHouse(secondPlayer) == true){
      playerTwoScore+=1;
    }
    if(playerOneScore > playerTwoScore){
      System.out.println("Player One wins!");
    }
    else if(playerOneScore == playerTwoScore){
      System.out.println("Tie");
    }
    else{
      System.out.println("Player Two wins!");
    }
  }
  //I write each method for the respective hand
 
  public static boolean pair(int[] hand){
    //checks for repeated numbers 
    for(int i = 0; i < 4; i++){
      for(int j = i+1; j < 5; j++){
        if((hand[i] % 13) == (hand[j] % 13)){
          return true;
        }        
      }
    }
    return false;    
  }
  public static boolean threeKind(int[] hand){
     //checks for 3 repeated numbers 
    for(int i = 0; i < 3; i++){
      for(int j = i+1; j < 4; j++){
        for(int k = j+1; k < 5; k++){
          if((hand[i] % 13 == hand[j] % 13) && (hand[i] % 13 == hand[k] % 13)){
            return true;
          }
        }
      }
    }
    return false;
  }
  public static boolean flush(int[] hand){
    int x = 0;
    //checks for same suit for all the numbers
    for(int i = 1; i < 5; i++){
      if( hand[x] / 13 != hand[i] / 13 ){
        return false;
      }
  }
    return true;
}
  public static boolean fullHouse(int[] hand){ 
    //first checks to see if there is a three of a kind then checks if there is a pair 
    for(int i = 0; i < 3; i++){
      for(int j = i+1; j < 4; j++){
        for(int k = j+1; k < 5; k++){
          if((hand[i] % 13 == hand[j] % 13) && (hand[i] % 13 == hand[k] % 13)){
            for(int x = 0; x < 4 ;x++){
                if(x != j || x != i || x != k){
                   for(int y = x+1; y < 5 ;y++){
                      if(y != j || y != i || y != k){
                        if(hand[x] % 13 == hand[y] %13){
                          return true;
                        }
                      }
                    }              
                  }
                }
              }
            }
          }
        }
    return false;
    } 
}