import java.util.Scanner;
import java.util.Random;

public class Lab08{
  public static void main(String[] args){
    Random rand = new Random();
    Scanner input = new Scanner(System.in);
    int randomStudents = rand.nextInt(5)+5;
    System.out.print("Please enter " + randomStudents + " students: ");
    String[] students = new String[randomStudents];
      for(int i = 0; i < randomStudents; i++){        
        students[i] = input.next();        
      }
    int[] midterms = new int[randomStudents];
    System.out.print("Please enter each Students' grade: ");
      for(int j = 0; j < randomStudents; j++){
        midterms[j] = input.nextInt();
      } 
      for(int k = 0; k < randomStudents; k++){
        System.out.println(students[k] + ": " + midterms[k]);
      }
    
  }
}