public class Cyclometer{
  //The start of every java program
  public static void main (String[] args) {
    //Input data for assignment
    int secsTrip1=480; //How many seconds did Trip 1 Take? 
      int secsTrip2=3220; //How many seconds did Trip 2 Take?
    int countsTrip1=1561; //How many wheel revolutions did Trip 1 make?
    int countsTrip2=9037; //How many wheel revolutions did Trip 2 make?
    double wheelDiameter=27.0, // gives diameter for the wheel
    PI=3.14159, // Pi is used for circumference of the wheel
  	feetPerMile=5280,  // Dimensional Analysis from Feet to Miles
  	inchesPerFoot=12,   // Dimensional Analysis from Inches to Feet
  	secondsPerMinute=60;  // Dimensional Analysis from Seconds to Minutes
       System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); // Prints how long Trip 1 took and how many revolutions it had
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // Prints how long Trip 2 took and how many revolutions it had
  double  	distanceTrip1=countsTrip1*wheelDiameter*PI; //Calculates the distance Trip 1 took based on 
    //how many revolutions the wheel took
distanceTrip1/= inchesPerFoot*feetPerMile; // Gives distanceTrip1 in miles
double	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Calculates the distance Trip 2 took based on
    //how many revolutions the wheel took.

	double totalDistance=distanceTrip1+distanceTrip2; //Adds the two distances together to get the total value. 
    //Using double accounts for the decimals that might be part of the calculation.
  System.out.println("Trip 1 was "+distanceTrip1+" miles "); //Prints Trip 1's distance in miles
	System.out.println("Trip 2 was "+distanceTrip2+" miles "); //Prints Trip 2's distance in miles
	System.out.println("The total distance was "+totalDistance+" miles "); //Prints the total distance in miles

      
  }//end of the program
}//end of class