//Jeffery Park
//Lehigh ID: jep321
//Hw 08: CSE2Linear.java

import java.util.Scanner; 
import java.util.Random;

public class CSE2Linear{
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    boolean intTrue = true;
    
    System.out.print("Enter 15 ascending integers from 0-100 for final grades in CSE2: ");
    int[] integers = new int[15];// I create a new array and input values 15 times inside the for-loop
    for(int i = 0; i < 15; i++){      
      do{
        intTrue = input.hasNextInt();
        if(intTrue == true){
          integers[i] = input.nextInt();
           if(integers[i] < 0 || integers[i] > 100){
             System.out.print("Error. Please enter positive integer values from 0 - 100: ");
             intTrue = false;
           }
        }
        else{
          System.out.print("Error. Please enter 15 INTEGER values from 0 - 100: ");
          String junk = input.next();
            }
      }while(!intTrue);
      for(int j = 0; j <= i; j++){
        if(integers[i] < integers[j]){
          System.out.print("Error. Please enter the integers in ascending order: ");
          integers[i] = input.nextInt();
        }
      }


    }
    for(int a = 0; a < 15; a++){
      System.out.print(integers[a] + " ");
    }

    binarySearch(integers);
    randomSearch(integers);

    
    
  }
  public static void binarySearch(int[] grades){
    Scanner input = new Scanner(System.in);
        System.out.print("Enter a grade to search for: ");
    int gradeSearch = input.nextInt();
    int y = 0;
    for (int x = 0; x < 15; x++){
      if(gradeSearch == grades[x]){
        System.out.println(gradeSearch + " is at position " + x);
        y++;
      }
      
    }
    if(y==0){
      System.out.println(gradeSearch + " was not found in this list.");
    }
  return;
}
  public static void randomSearch(int[] grades){
    Random rand = new Random();
    Scanner input = new Scanner(System.in);    
    int[] scrambledNumbers = new int[15];
    
    
    for(int i = 0 ; i < grades.length ; i++){
      int randomGenerator = rand.nextInt(15);
      if(scrambledNumbers[randomGenerator] == 0){
      scrambledNumbers[randomGenerator] = grades[i];
      }
      else if (i > 0){//decrements if an array space is already filled  until a new space is found
        i--;
      }
      
    }
    for(int a = 0; a < 15; a++){
      System.out.print(scrambledNumbers[a] + " ");
    }
        System.out.print("Enter a grade to search for: ");
    int gradeSearch = input.nextInt();
    int y = 0;
    for (int x = 0; x < 15; x++){
      if(gradeSearch == scrambledNumbers[x]){
        System.out.println(gradeSearch + " is at position " + x);
        y++;
      }
      
    }
    if(y==0){
      System.out.println(gradeSearch + " was not found in this list.");
    }
  return;
    
  }
  
}

