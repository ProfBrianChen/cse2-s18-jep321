//Jeffery Park
//Hw 10 ROBOT CITY
//jep321
import java.util.Random;
public class RobotCity{
  public static void main(String[] args){  
    Random rand = new Random();
    int width = rand.nextInt((15 - 10) + 1) + 10; //ROW
    int height = rand.nextInt((15 - 10) + 1) + 10; //COLUMN
    int[][] cityArray = new int[width][height]; //INITIALIZE THE MATRIX
    cityArray = buildCity(cityArray); //SETS THE MATRIX EQUAL TO THE BUILDCITY METHOD 
    display(cityArray);
    System.out.println(); 
    cityArray = invade(cityArray , 10);
    display(cityArray);
    System.out.println();
    for(int i = 0; i < 5; i++){//DISPLAYS THE UPDATE METHOD 5 TIMES
    cityArray = update(cityArray);
    display(cityArray);
    System.out.println();
    }      
  }
  public static void display(int[][] input){//prints the wanted matrix
    for(int i = 0; i < input.length; i++){
      for(int j = 0; j< input[i].length; j++){
        System.out.printf("%6d", input[i][j]);
      }
      System.out.println();
    }
  }
  public static int[][] buildCity(int[][] input){//sets random integers to the array indices
    Random rand = new Random();
    for(int i = 0; i < input.length; i++){
      for(int j = 0; j< input[i].length; j++){
        input[i][j] = rand.nextInt((999-100) + 1) + 100;
      }
    }
    return input;
  }
  public static int[][] invade(int[][] input, int k){//looks for a positive number and skips the negative numbers
    Random rand = new Random();

    for(int i = 1; i <= k; i++){
    int height = rand.nextInt(input[0].length);
    int width = rand.nextInt(input.length);
      if( input[width][height]>0){
        input[width][height] = -input[width][height];
      }
      else if( input[width][height] < 0 ){
        i--; //resets counter since it already has a robot in it
      }
    }
      return input;
  }
  public static int[][] update(int[][] input){//checks to see if there is a negative value present and a positive value present to the right of the negative value.
    for(int i = 0; i < input.length; i++){
      for(int j = 0; j < input[i].length-1; j++){
        if(((input[i][j]) < 0) && (input[i][j+1] > 0)){
          input[i][j+1] = -input[i][j+1];
          j++;
        }     
      }
    }         
    return input;
  }
}