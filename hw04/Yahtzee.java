//Name:Jeffery Park
//Lehigh ID: jep321
// Hw04

import java.util.Scanner;

public class Yahtzee{
  public static void main(String[] args){
    //upper section
  Scanner input = new Scanner(System.in);
    System.out.print("Roll randomly? or Input numbers? Answer 1 for random and 2 to choose numbers ");
    int decision = input.nextInt();
    int aces = 0;
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    int dice1; 
    int dice2;
    int dice3;
    int dice4;
    int dice5; 
    
    int choose1;
    int choose2; 
    int choose3;
    int choose4; 
    int choose5;
    
    switch (decision){
      case 1: 
       dice1 = (int)(Math.random()*5)+1;
      System.out.println("You rolled a: " + dice1);
        if (dice1 == 1){
          aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        if (dice1 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
        }
        if (dice1 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (dice1 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (dice1 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (dice1 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
        
        
        
      
    
       dice2 = (int)(Math.random()*5)+1;
      System.out.println("You rolled a: " + dice2);
        if (dice2 == 1){
          aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        else if (dice2 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
        }
        else if (dice2 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        else if (dice2 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        else if (dice2 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        else if (dice2 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
      
        
    
       dice3 = (int)(Math.random()*5)+1;
      System.out.println("You rolled a: " + dice3);
        if (dice3 == 1){
          aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        else if (dice3 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
          
        }
        else if (dice3 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        else if (dice3 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        else if (dice3 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        else if (dice3 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
      
        
    
       dice4 = (int)(Math.random()*5)+1;
      System.out.println("You rolled a: " + dice4);
        if (dice4 == 1){
          aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        else if (dice4 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
        }
        else if (dice4 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        else if (dice4 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        else if (dice4 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        else if (dice4 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
      
        
    
       dice5 = (int)(Math.random()*5)+1;
      System.out.println("You rolled a: " + dice5);
        if (dice5 == 1){
          aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        else if (dice5 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
        }
        else if (dice5 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        else if (dice5 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        else if (dice5 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        else if (dice5 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
      
        
        
        break;
      case 2:
        System.out.print("Input number of the first dice from 1 - 6: ");
           choose1 = input.nextInt();
        if ( choose1 <= 0 || choose1 > 6 ){
          System.out.println("Invalid choice! Program will now exit...");
        System.exit(0);
        }
        else{
        System.out.println("You chose: " + choose1);
        }
        if (choose1 == 1){
        aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        if (choose1 == 2){
          twos = twos + 1;
           System.out.println("Twos:" + twos);
        }
        if (choose1 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (choose1 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (choose1 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (choose1 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
        
        System.out.print("Input number of the second dice from 1 - 6: ");
           choose2 = input.nextInt();
        if ( choose2 <= 0 || choose2 > 6 ){
          System.out.println("Invalid choice! Program will now exit...");
        System.exit(0);
        }
        else{
        System.out.println("You chose: " + choose2);
        }
        if (choose2 == 1){
        aces = aces + 1;
          System.out.println("Aces:" + aces);
        }
        if (choose2 == 2){
          twos = twos + 1;
          System.out.println("Twos:" + twos);
        }
        if (choose2 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (choose2 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (choose2 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (choose2 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
      
        
        
        System.out.print("Input number of the third dice from 1 - 6: ");
           choose3 = input.nextInt();
        if ( choose3 <= 0 || choose3 > 6 ){
          System.out.println("Invalid choice! Program will now exit...");
        System.exit(0);
        
        }
        else{
        System.out.println("You chose: " + choose3);
        }
        if (choose3 == 1){
        aces = aces + 1;
          System.out.println("Aces:" + aces);
      }
        if (choose3 == 2){
          twos = twos + 1;
           System.out.println("Twos:" + twos);
        }
        if (choose1 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (choose3 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (choose3 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (choose3 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
        
        System.out.print("Input number of the fourth dice from 1 - 6: ");
           choose4 = input.nextInt();
        if ( choose4 <= 0 || choose4 > 6 ){
          System.out.println("Invalid choice! Program will now exit...");
        System.exit(0);
        }
        else{
        System.out.println("You chose: " + choose4);
        }
        if (choose4 == 1){
        aces = aces + 1;
          System.out.println("Aces:" + aces);
      }
        if (choose4 == 2){
          twos = twos + 1;
           System.out.println("Twos:" + twos);
        }
        if (choose4 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (choose4 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (choose4 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (choose4 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
        
        System.out.print("Input number of the fifth dice from 1 - 6: ");
           choose5 = input.nextInt();
        if ( choose5 <= 0 || choose5 > 6 ){
          System.out.println("Invalid choice! Program will now exit...");
        System.exit(0);
        }
        else{
        System.out.println("You chose: " + choose5);
        }
        if (choose5 == 1){
        aces = aces + 1;
          System.out.println("Aces:" + aces);
      }
        if (choose5 == 2){
          twos = twos + 1;
           System.out.println("Twos:" + twos);
        }
        if (choose5 == 3){
          threes = threes + 1;
          System.out.println("Threes:" + threes);
        }
        if (choose5 == 4){
          fours = fours + 1;
          System.out.println("Fours:" + fours);
        }
        if (choose5 == 5){
          fives = fives + 1;
          System.out.println("Fives:" + fives);
        }
        if (choose5 == 6){
          sixes = sixes + 1;
          System.out.println("Sixes:" + sixes);
        }
        
        break;
        
    }
// lower section
    
    
    
      
     
      
    
   
     
      
      
    
    
    
      
  
   
      
      
   
    
      
      
    
    
   
 
    
    
    
    
    
    
  }
}